# WebSocket Server & Client

This project implements a websocket server in Deno and websocket client app in SvelteJS.

## Building and running in production mode

To create an optimised version of the app:

``` bash
npm run build
```

# How to use

Run the following:

``` bash
npm install
npm run server # runs the websocket server
npm run dev    # runs the websocket client
```

Navigate to [localhost:5000](http://localhost:5000). You should see your app running.

## Details

This project is derived from [SpinSpire Svelte template](https://bitbucket.org/spinspire/sveltejs-template). 
So please see the [README](https://bitbucket.org/spinspire/sveltejs-template/src/master/README.md) file
of that project.
