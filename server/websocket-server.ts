#!/usr/bin/env -S deno run --allow-net

import { serve } from "https://deno.land/std/http/server.ts";
import { parse } from "https://deno.land/std/flags/mod.ts";
import {
  acceptWebSocket,
  isWebSocketCloseEvent,
  WebSocket,
} from "https://deno.land/std/ws/mod.ts";

/*
 * WebSocket server example in Deno.
 * See: https://deno.land/std/ws/example_server.ts
 */

/*
 * Parse command line args and options and destructure
 * --port <port> arg into port variable (with a default
 * value of 8080)
 */
const { port = 8080 } = parse(Deno.args);

const socks: WebSocket[] = [];

function removeSock(sock: WebSocket) {
  const index = socks.indexOf(sock);
  if (index >= 0) {
    socks.splice(index, 1);
  }
  console.log("total connections--", socks.length);
}

// send the given message to all connected WebSocket clients
function broadcast(msg: string) {
  for (const s of socks) {
    if (s.isClosed) {
      removeSock(s);
    } else {
      s.send(msg);
    }
  }
}

console.log(`Listening on port ${port}`);
// listen on the port and use the async iterator for request processing
for await (const req of serve({ port })) {
  const { conn, r: bufReader, w: bufWriter, headers } = req;
  try {
    // use the HTTP req members to accept websocket connection
    acceptWebSocket({ conn, bufReader, bufWriter, headers }).then(
      async (sock) => {
        socks.push(sock);
        console.debug("total connections++", socks.length);
        // add to collection of subscribers
        try {
          for await (const msg of sock) {
            if (typeof msg === "string") {
              console.debug("message", msg);
              broadcast(msg);
            } else if (isWebSocketCloseEvent(msg)) {
              removeSock(sock);
            } else {
              // other possibilities: binary message, ping event, close event
            }
          }
        } catch (e) {
          console.error("Failed to receive message", e);
          removeSock(sock);
          if (!sock.isClosed) {
            await sock.close().catch(console.error);
          }
        }
      },
    );
  } catch (e) {
    console.error("Failed to accept websocket", e);
    await req.respond({ status: 400 });
  }
}
